.\" -*- nroff -*-
.TH SELECT-EDITOR 1 "21 May 2008" "Debian"
.SH NAME
select-editor \- select your default sensible-editor from all installed editors
.SH SYNOPSIS
.B select-editor
.SH DESCRIPTION
.B select-editor
provides a coherent mechanism for selecting and storing a preferred
sensible-editor on a per-user basis.
It lists the available editors on a system and interactively prompts
the user to select one.
The results are stored as
.B SELECTED_EDITOR
variable in \%\s+3~\s-3/.selected_editor, which is sourced and used by
.BR sensible-editor (1)
command.
\%SELECTED_EDITOR
variable is overridden by the 
.B VISUAL
and
.B EDITOR
environment variables.
.SH AUTHOR
.B select-editor
was written by Dustin Kirkland <kirkland@canonical.com>.
.SH "SEE ALSO"
.BR sensible-editor (1)
