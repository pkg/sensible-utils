.\" -*- nroff -*-
.TH SENSIBLE-TERMINAL 1 "28 Aug 2022" "Debian"
.SH NAME
sensible-terminal \- sensible terminal emulator
.SH SYNOPSIS
.B sensible-terminal 
.RI [-T title] [--wait] [-e cmd...]
.SH DESCRIPTION
.B sensible-terminal-emulator
makes sensible decisions on which terminal emulator to call.
Programs in Debian can use this script
as their default terminal emulator or emulate their behavior.
.B TERMINAL_EMULATOR
environment variable could be set, and will be used if set.
Any string acceptable as a command_string operand to the
.B sh -c
command shall be valid.
.SH "STANDARD"
Documentation of behavior of
.B sensible-utils
under a debian system is available under
sections 11.4-11.8 of debian-policy usually installed under
/usr/share/doc/debian-policy (you might need to install debian-policy)
